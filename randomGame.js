
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function generateNewNumbers(){

    var r = getRandomInt(1,5);
    var number1;
    var number2;
    if(parseInt(r)==1){
        number1 = getRandomInt(0, 101);
        number2 = getRandomInt(0, 101 - number1);
        document.getElementById("operator").innerHTML="+";
        document.getElementById("num1").innerHTML = number1;
    }else if(parseInt(r)==2){
        number1 = getRandomInt(0, 101);
        number2 = getRandomInt(0, number1);
        document.getElementById("operator").innerHTML="-";
        document.getElementById("num1").innerHTML = number1;
    }else if(parseInt(r)==3){
        number1 = getRandomInt(0, 11);
        number2 = getRandomInt(0, 11);
        document.getElementById("operator").innerHTML="x";
        document.getElementById("num1").innerHTML = number1;
    }else{
        number1 = getRandomInt(0, 11);
        number2 = getRandomInt(1, 11);
        document.getElementById("operator").innerHTML="/";
        document.getElementById("num1").innerHTML = number1*number2;
    }
	document.getElementById("answer").value = "";
    document.getElementById("num2").innerHTML = number2;
}


var score = 0;
var lives = 3;
var numOfExercises = 10;
var completeExercises = 0;

generateNewNumbers();

var number1 = document.getElementById("num1").innerHTML;
var number2 = document.getElementById("num2").innerHTML;
if(document.getElementById("operator").innerHTML === "+"){
    correct = parseInt(number1) + parseInt(number2);
}else if(document.getElementById("operator").innerHTML === "-"){
    correct = parseInt(number1) - parseInt(number2);
}else if(document.getElementById("operator").innerHTML === "x"){
    correct = parseInt(number1) * parseInt(number2);
}else{
    correct = parseInt(number1) / parseInt(number2);
}

function checkResult() {

    var number1 = document.getElementById("num1").innerHTML;
    var number2 = document.getElementById("num2").innerHTML;
    if(document.getElementById("operator").innerHTML === "+"){
        correct = parseInt(number1) + parseInt(number2);
    }else if(document.getElementById("operator").innerHTML === "-"){
        correct = parseInt(number1) - parseInt(number2);
    }else if(document.getElementById("operator").innerHTML === "x"){
        correct = parseInt(number1) * parseInt(number2);
    }else{
        correct = parseInt(number1) / parseInt(number2);
    }


    var test = document.getElementById("answer").value;

    if (test == correct) {

        score += 10;
        document.getElementById("score").innerHTML = score;

        alert("Correct");

        completeExercises++;
        $('.progress-bar').css("width", completeExercises/numOfExercises*100+"%");
        document.getElementById("answer").value=0;

        if(completeExercises == numOfExercises){
            alert("You are a genius. Good job!");
            $("#finish").show();
        }else{
            generateNewNumbers();
        }



    } else {


        $("#health" + lives).removeClass("rocketHealth");
        lives--;
        alert("Wrong answer. Try again!");
        if (lives <= 0) {
            alert("Game over!");
            window.location.replace("gameCategory.html")

        }
        generateNewNumbers();
    }
}



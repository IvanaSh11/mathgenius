
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function generateNewNumbers(){
    var number1 = getRandomInt(0, 101);
    var number2 = getRandomInt(0, number1);
    document.getElementById("num1").innerHTML = number1;
    document.getElementById("num2").innerHTML = number2;
document.getElementById("answer").value = "";
}


var number1 = getRandomInt(0, 101);
var number2 = getRandomInt(0, number1);


var op = "-";

var score = 0;
var lives = 3;
var numOfExercises = 10;
var completeExercises = 0;


document.getElementById("num1").innerHTML = number1;
document.getElementById("num2").innerHTML = number2;
document.getElementById("operator").innerHTML=op;
correct = parseInt(number1) - parseInt(number2);

function checkResult() {
    var number1 = document.getElementById("num1").innerHTML;
    var number2 = document.getElementById("num2").innerHTML;
    correct = parseInt(number1) - parseInt(number2);

    var test = document.getElementById("answer").value;

    if (test == correct) {

        score += 10;
        document.getElementById("score").innerHTML = score;

        alert("Correct");

        completeExercises++;
        $('.progress-bar').css("width", completeExercises/numOfExercises*100+"%");
        document.getElementById("answer").value=0;

        if(completeExercises == numOfExercises){
            alert("You are a genius. Good job!");
            $("#finish").show();
        }else{
            generateNewNumbers();
        }



    } else {


        $("#health" + lives).removeClass("rocketHealth");
        lives--;
        alert("Wrong answer. Try again!");
        if (lives <= 0) {
            alert("Game over!");
            window.location.replace("gameCategory.html")

        }
        generateNewNumbers();
    }
}


